require('./part/init-swiper.js');
require('./part/init-event-calendar.js');   
require('./part/init-menu.js');  


$(function(){
    
    let $date1 = $('#data1');
    let $date2 = $('#data2');
    let startDate = $date1.val();
    let endDate = $date2.val();
    let $form = $('.js-event-form')
    let firstSelected = false

    let dp = $('#calendar').datepicker({
        multipleDatesSeparator: ' - ',
        range: true,
        clearButton: true,
        onSelect(formattedDate, date, inst) {
            if( !date ){
                $date1.remove()
                $date2.remove()
                $form.trigger('submit')
            }
            if( inst.selectedDates.length < 2 ) return

            let _tmpDate1 = inst.selectedDates[0]
            let _tmpDate2 = inst.selectedDates[1]

            $date1.val( [_tmpDate1.getFullYear(),_tmpDate1.getMonth()+1,_tmpDate1.getDate()].join('.') )
            $date2.val( [_tmpDate2.getFullYear(),_tmpDate2.getMonth()+1,_tmpDate2.getDate()].join('.') )

            if( firstSelected ) $form.trigger('submit')
            if( !startDate && !endDate ) $form.trigger('submit')
        },

    }).data('datepicker');

    if( startDate && endDate ){
        dp.selectDate([new Date(startDate), new Date(endDate)]);
        firstSelected = true
    }


       //select2
       $('.js-example-basic-single').select2();    

       //FlipBook
       //$('.sample-container').FlipBook({pdf: 'img/CondoLiving.pdf'});
    $(document).on('change', '#event-type', function(e){
        var val = $("#event-type").val();
        const url = new URL(window.location); 
        url.searchParams.set('sort', val); 
        // history.pushState(null, null, url); 
        
    });
    
    $('.js-event-form input, .js-event-form select').change(function(){
        $('.js-event-form').trigger('submit')
    })


    

})

 