const mobBtn = document.querySelector('.btn__mob');
const nav = document.querySelector('.nav');
const overlay = document.querySelector('.overlay');
const navArrow = document.querySelectorAll('.nav__arrow');
const subLists = document.querySelectorAll('.nav ul');
const btnsBack= document.querySelectorAll('.nav__list-back');
const search= document.querySelector('.search-header__click');
const searchWrap= document.querySelector('.search-header');


search.addEventListener('click', function(){
    searchWrap.classList.toggle('js-show');
})

navArrow.forEach(element => {
    element.addEventListener('click', function(e){
        this.closest("li").lastElementChild.classList.add("js-show"); 
    })
});

mobBtn.addEventListener('click', function(){  
    if(this.classList.contains('js-show')){
        menuHide();
    }else {
        menuShow();
    } 
}); 

function menuShow(){ 
    mobBtn.classList.add('js-show');
    nav.classList.add('js-show');
    overlay.classList.add('js-show');
}
function menuHide(){ 
    mobBtn.classList.remove('js-show');
    nav.classList.remove('js-show');
    overlay.classList.remove('js-show');
    subLists.forEach(el => {
        el.classList.remove('js-show');
    })
}

window.addEventListener('click', function (e) {
    if (!nav.contains(e.target) && !mobBtn.contains(e.target)) {
        menuHide();
    }
    if(!searchWrap.contains(e.target) && !search.contains(e.target)){
        searchWrap.classList.remove('js-show');
    }
}); 

btnsBack.forEach(el => {
    el.addEventListener('click', function(){  
        this.closest('ul').classList.remove('js-show'); 
    }); 
}) 