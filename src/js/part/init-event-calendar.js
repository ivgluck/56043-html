$(function(){ 
    // let data = [
    //     { "date": "2021-12-21 10:00:00",  "title": "MVC: Зерно-Комбикорма-Ветеринария 2021", "description": "Выставка «MVC: Зерно-Комбикорма-Ветеринария 2021", "url": "event-singl.html" },
    //     { "date": "2021-12-25 10:00:00",  "title": "Матч по волейболу команд ВЕТПРИБОР и Логистика МСК", "description": "хоть и набрал больше всех очков (шесть), не смог забить все, что ему прилетало.", "url": "event-singl.html" },
    //     { "date": "2021-12-31 10:00:00",  "title": "MVC: Зерно-Комбикорма-Ветеринария 2021", "description": "«MVC: Зерно-Комбикорма-Ветеринария 2021", "url": "event-singl.html" },
    //     { "date": "2022-01-01 10:00:00",  "title": "Матч по волейболу команд ВЕТПРИБОР и Логистика МСК", "description": "хоть и набрал больше всех очков (шесть), не смог забить все, что ему прилетало.", "url": "event-singl.html" },
    // ];

    $.ajax({
        url: 'ajax/calendar.php',
        cache: false,
        dataType: "json",
        success: function(data) {
            $('#event').eventCalendar({  
                jsonData: data,
                jsonDateFormat: 'human',
                showDescription: true,
                openEventInNewWindow: true,
                dateFormat: 'DD-MM-YYYY',
                locales: {
                    locale: "ru",
                    txt_noEvents: "Нет запланированных событий",
                    txt_SpecificEvents_prev: "",
                    txt_SpecificEvents_after: "события:", 
                    txt_NextEvents: "Следующие события",
                    txt_GoToEventUrl: "Смотреть",
                    moment: {
                        "months" : [ "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь",
                                "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" ],
                        "monthsShort" : [ "Янв", "Фев", "Мар", "Апр", "Май", "Июн",
                                "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек" ],
                        "weekdays" : [ "Воскресенье","Понедельник","Вторник","Среда",
                                "Четверг","Пятница","Суббота" ],
                        "weekdaysShort" : [ "Вс","Пн","Вт","Ср",
                                "Чт","Пт","Сб" ],
                        "weekdaysMin" : [ "Вс","Пн","Вт","Ср",
                        "Чт","Пт","Сб" ]
                    }
                  }
            }); 
          }
    });  

});