const {src, dest} = require('gulp');

//add config
const path = require('./../config/path.js');

//Plugins
const newer = require('gulp-newer'); //Minify only new image files




//task
const copyFile = () => {
    return src(path.copyFiles.src)
        .pipe(dest(path.copyFiles.dest))
}

module.exports = copyFile;